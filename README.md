# Point of sale
I started this project a year ago to learn c#, WPF, SQL server and multitier architecture. 
The main hurdle in completing this project is that I moved to linux. I made it public so someone can contribute to it.

# TODO
- [ ] Complete sale module.
- [ ] Improve UI.
- [ ] Clean code.

# Screenshots

![Login](img/login.PNG)

![Stock](img/stock.PNG)

![user](img/user.PNG)
